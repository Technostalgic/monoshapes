﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoShapes
{
	public class ShapeRenderer
	{
		public GraphicsDevice GraphicsDevice { get; private set; }
		public BasicEffect Effect { get; private set; }

		public ShapeRenderer(GraphicsDevice graphicsDevice)
		{
			GraphicsDevice = graphicsDevice;
			Effect = new BasicEffect(GraphicsDevice);
			Effect.VertexColorEnabled = true;
			Effect.LightingEnabled = false;
			Effect.View = Matrix.CreateLookAt(Vector3.Backward * 10, Vector3.Zero, Vector3.Up);
			Effect.Projection = Matrix.CreateOrthographic(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 1000);
		}

		private float AngleDif(float a, float b)
		{
			float twoPi = (float)Math.PI * 2;
			float r = b - a; 
			r += (float)Math.PI;
			r %= twoPi;
			if (r < 0) r += twoPi;
			r -= (float)Math.PI;
			return r;
		}

		public void DrawBoxFill(Vector2 position, Vector2 size, Color color)
		{
			Vector2 min = position;
			Vector2 max = position + size;

			VertexPositionColor[] verts = new VertexPositionColor[4];
			verts[0] = new VertexPositionColor(new Vector3(min.X, min.Y, 0), color);
			verts[1] = new VertexPositionColor(new Vector3(max.X, min.Y, 0), color);
			verts[2] = new VertexPositionColor(new Vector3(min.X, max.Y, 0), color);
			verts[3] = new VertexPositionColor(new Vector3(max.X, max.Y, 0), color);

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, verts, 0, 2, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawBoxOutline(Vector2 position, Vector2 size, Color color, float thickness = 1)
		{
			float halfThick = thickness * 0.5f;
			Vector2 minOuter = position - new Vector2(halfThick);
			Vector2 minInner = position + new Vector2(halfThick);
			Vector2 maxOuter = position + size + new Vector2(halfThick);
			Vector2 maxInner = position + size - new Vector2(halfThick);

			VertexPositionColor[] verts = new VertexPositionColor[10];
			verts[0] = new VertexPositionColor(new Vector3(minInner.X, minInner.Y, 0), color);
			verts[1] = new VertexPositionColor(new Vector3(minOuter.X, minOuter.Y, 0), color);
			verts[2] = new VertexPositionColor(new Vector3(maxInner.X, minInner.Y, 0), color);
			verts[3] = new VertexPositionColor(new Vector3(maxOuter.X, minOuter.Y, 0), color);
			verts[4] = new VertexPositionColor(new Vector3(maxInner.X, maxInner.Y, 0), color);
			verts[5] = new VertexPositionColor(new Vector3(maxOuter.X, maxOuter.Y, 0), color);
			verts[6] = new VertexPositionColor(new Vector3(minInner.X, maxInner.Y, 0), color);
			verts[7] = new VertexPositionColor(new Vector3(minOuter.X, maxOuter.Y, 0), color);
			verts[8] = new VertexPositionColor(new Vector3(minInner.X, minInner.Y, 0), color);
			verts[9] = new VertexPositionColor(new Vector3(minOuter.X, minOuter.Y, 0), color);

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, verts, 0, 8, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawEllipseFill(Vector2 position, float radius, Color color, int segments = 12)
		{
			DrawEllipseFill(position, new Vector2(radius), color, segments);
		}

		public void DrawEllipseFill(Vector2 position, Vector2 radius, Color color, int segments = 12)
		{
			VertexPositionColor[] verts = new VertexPositionColor[segments];
			for (int i = 0; i < segments; i++)
			{
				float angle = ((float)i / segments) * (float)Math.PI * 2f;
				verts[i].Position = new Vector3(
					position.X + (float)Math.Cos(angle) * radius.X, 
					position.Y + (float)Math.Sin(angle) * radius.Y, 0
				);
				verts[i].Color = color;
			}

			int[] tris = new int[segments * 3];
			for(int i = 0; i < segments; i++)
			{
				int i3 = i * 3;
				tris[i3 + 0] = 0;
				tris[i3 + 1] = (i + 1) % segments;
				tris[i3 + 2] = (i + 2) % segments;
			}

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verts, 0, verts.Length, tris, 0, tris.Length / 3, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawEllipseOutline(Vector2 position, Vector2 radius, Color color, int segments = 12, float thickness = 1)
		{
			float halfThick = thickness * 0.5f;

			VertexPositionColor[] verts = new VertexPositionColor[segments * 2];
			for (int i = 0; i < segments; i++)
			{
				float angle = ((float)i / segments) * (float)Math.PI * 2f;
				int i2 = i * 2;

				verts[i2].Position = new Vector3(
					position.X + (float)Math.Cos(angle) * (radius.X - halfThick),
					position.Y + (float)Math.Sin(angle) * (radius.Y - halfThick), 0
				);
				verts[i2 + 1].Position = new Vector3(
					position.X + (float)Math.Cos(angle) * (radius.X + halfThick),
					position.Y + (float)Math.Sin(angle) * (radius.Y + halfThick), 0
				);

				verts[i].Color = color;
				verts[i + 1].Color = color;
			}
			verts[verts.Length - 2] = verts[0];
			verts[verts.Length - 1] = verts[1];

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, verts, 0, verts.Length - 2, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawLine(Vector2 start, Vector2 end, Color color, float thickness = 1)
		{
			float halfThick = thickness * 0.5f;
			Vector2 dif = start - end;
			float angle = (float)Math.Atan2(dif.Y, dif.X);
			float a1 = angle - (float)Math.PI * 0.5f;
			float a2 = angle + (float)Math.PI * 0.5f;

			Vector2 s1 = start + new Vector2((float)Math.Cos(a1), (float)Math.Sin(a1)) * halfThick;
			Vector2 s2 = start + new Vector2((float)Math.Cos(a2), (float)Math.Sin(a2)) * halfThick;
			Vector2 e1 = end + new Vector2((float)Math.Cos(a1), (float)Math.Sin(a1)) * halfThick;
			Vector2 e2 = end + new Vector2((float)Math.Cos(a2), (float)Math.Sin(a2)) * halfThick;

			VertexPositionColor[] verts = new VertexPositionColor[4];
			verts[0] = new VertexPositionColor(new Vector3(s2, 0), color);
			verts[1] = new VertexPositionColor(new Vector3(s1, 0), color);
			verts[2] = new VertexPositionColor(new Vector3(e2, 0), color);
			verts[3] = new VertexPositionColor(new Vector3(e1, 0), color);

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, verts, 0, 2, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawPolyLine(Vector2[] points, Color color, float thickness = 1)
		{
			float halfThick = thickness * 0.5f;
			float halfPi = (float)Math.PI * 0.5f;

			VertexPositionColor[] verts = new VertexPositionColor[points.Length * 2];
			float prevLineDir = 0;
			for(int i = 0; i < points.Length; i++)
			{
				int i2 = i * 2;
				verts[i2].Color = color;
				verts[i2 + 1].Color = color;

				Vector2 curPoint = points[i];
				Vector2 nextPoint = points[(i + 1) % points.Length];
				if (i < points.Length - 1)
					prevLineDir = (nextPoint - curPoint).Direction();
				float p1Dir = prevLineDir + halfPi;

				verts[i2].Position = new Vector3(
					curPoint.X + (float)Math.Cos(p1Dir) * halfThick,
					curPoint.Y + (float)Math.Sin(p1Dir) * halfThick, 0
				);
				verts[i2 + 1].Position = new Vector3(
					curPoint.X + (float)Math.Cos(p1Dir) * -halfThick,
					curPoint.Y + (float)Math.Sin(p1Dir) * -halfThick, 0
				);
			}

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, verts, 0, verts.Length - 2, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawPolygonFill(Vector2[] points, Color color)
		{
			VertexPositionColor[] verts = new VertexPositionColor[points.Length];
			for (int i = verts.Length - 1; i >= 0; i--)
				verts[i] = new VertexPositionColor(new Vector3(points[i], 0), color);

			int[] tris = Triangulator.Triangulate(points);
			int triCount = tris.Length / 3;

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, verts, 0, verts.Length, tris, 0, triCount, VertexPositionColor.VertexDeclaration);
			}
		}

		public void DrawPolygonOutline(Vector2[] points, Color color, float thickness = 1)
		{
			float halfThick = thickness * 0.5f;

			VertexPositionColor[] verts = new VertexPositionColor[points.Length * 2 + 2];
			for(int i = points.Length; i >= 0; i--)
			{
				Vector2 curPoint = points[i % points.Length];
				Vector2 nextPoint = points[(i + 1) % points.Length];
				Vector2 nextDif = (nextPoint - curPoint);
				Vector2 prevPoint = points[(i - 1 + points.Length) % points.Length];
				Vector2 prevDif = (prevPoint - curPoint);
				Vector2 dif = ((nextDif.Normalized() + prevDif.Normalized()) * 0.5f);
				dif.Normalize();
				dif *= halfThick;

				float angDif = AngleDif(prevDif.Direction(), nextDif.Direction());
				if (angDif > 0)
					dif *= -1;

				Vector2 inner = curPoint - dif;
				Vector2 outer = curPoint + dif;
				int i2 = i * 2;
				verts[i2] = new VertexPositionColor(new Vector3(inner, 0), color);
				verts[i2 + 1] = new VertexPositionColor(new Vector3(outer, 0), color);
			}

			foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, verts, 0, verts.Length - 2, VertexPositionColor.VertexDeclaration);
			}
		}
	}
}
