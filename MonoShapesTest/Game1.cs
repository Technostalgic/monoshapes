﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Framework;
using MonoShapes;

namespace MonoShapesTest
{
	public class Game1 : Game
	{
		private GraphicsDeviceManager _graphics;
		private SpriteBatch _spriteBatch;
		private BasicEffect _effect;
		private VertexPositionColor[] _verts;
		private Matrix _view;
		private Matrix _projection;
		private Texture2D _pixel;
		private ShapeRenderer _shapeRenderer;

		public Game1()
		{
			_graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
			IsMouseVisible = true;
		}

		protected override void Initialize()
		{
			base.Initialize();

			_verts = new VertexPositionColor[3]
			{
				new VertexPositionColor(new Vector3(0, 50f, 0), new Color(0, 0, 0, 0)),
				new VertexPositionColor(new Vector3(50f, 0, 0), Color.Green),
				new VertexPositionColor(new Vector3(-50f, 0, 0), Color.Blue)
			};
		}

		protected override void LoadContent()
		{
			_spriteBatch = new SpriteBatch(GraphicsDevice);
			_effect = new BasicEffect(GraphicsDevice);

			_view = Matrix.CreateLookAt(new Vector3(0, 0, 0), Vector3.Forward, Vector3.Up);
			_projection = Matrix.CreateOrthographic(
				_graphics.PreferredBackBufferWidth, 
				_graphics.PreferredBackBufferHeight, 
				0, 1000
			);

			_pixel = new Texture2D(GraphicsDevice, 1, 1);
			_pixel.SetData(new Color[1] { Color.White });

			_shapeRenderer = new ShapeRenderer(GraphicsDevice);
		}

		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			RasterizerState rs = new RasterizerState();
			rs.CullMode = CullMode.None;
			GraphicsDevice.RasterizerState = rs;

			Vector2[] poly = new Vector2[] {
				new Vector2(-150, -50),
				new Vector2(150, -50),
				new Vector2(125, -175),
				new Vector2(0, -100),
				new Vector2(-135, -170)
			};

			Vector2[] polyLine = new Vector2[] { 
				new Vector2(150, 150),
				new Vector2(175, 100),
				new Vector2(225, 25),
				new Vector2(200, -50),
				new Vector2(215, -200)
			};

			_shapeRenderer.DrawBoxFill(Vector2.Zero, Vector2.One * 100, Color.DarkGreen);
			_shapeRenderer.DrawBoxOutline(Vector2.Zero, Vector2.One * 100, Color.Black, 10);
			_shapeRenderer.DrawLine(Vector2.One * -100, Vector2.Zero, Color.DarkRed, 10);
			_shapeRenderer.DrawPolygonFill(poly, Color.Blue);
			_shapeRenderer.DrawPolygonOutline(poly, Color.Black, 12);
			_shapeRenderer.DrawEllipseFill(new Vector2(-250, 0), new Vector2(50, 100), Color.Yellow, 12);
			_shapeRenderer.DrawEllipseOutline(new Vector2(-250, 0), new Vector2(50, 100), Color.Black, 48, 10);
			_shapeRenderer.DrawPolyLine(polyLine, Color.Goldenrod, 10);
			

			//_effect.World = Matrix.Identity;
			//_effect.View = _view;
			//_effect.Projection = _projection;
			//_effect.VertexColorEnabled = true;
			//
			//foreach (EffectPass pass in _effect.CurrentTechnique.Passes)
			//{
			//	pass.Apply();
			//	GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, _verts, 0, 1, VertexPositionColor.VertexDeclaration);
			//}

			base.Draw(gameTime);
		}
	}
}
